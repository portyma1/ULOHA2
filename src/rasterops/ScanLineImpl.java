package rasterops;

import objectdata.Edge;
import objectdata.Point2D;
import objectdata.Polygon2D;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanLineImpl <P> implements ScanLine<P>{

    @Override
    public void fill(final @NotNull Polygon2D polygon2D, final @NotNull RasterImage<P> img,
                     final @NotNull Polygoner2D<P> polygoner2D, final @NotNull P areaPixel, final @NotNull P polygonPixel, final @NotNull Liner<P> liner) {

        //initialize an empty list of edges

        /**
         * Vyplňování listu hran
         *
         * @MartinPortych
         */
        List<Edge> edges= new ArrayList<Edge>();

        int listSize = polygon2D.getPoints().size();
        if (listSize==0){
            System.err.println("Nakresli polygon");
            return;}
        for (int i = 0; i < listSize; i++) {
            int next = (i + 1) % listSize;
            int y1=polygon2D.getPoint(i).getR1();
            int y2=polygon2D.getPoint(next).getR1();

            if (y1!=y2){ //isHorizontal
                Edge edge=new Edge(new Point2D(polygon2D.getPoint(i).getC1(),polygon2D.getPoint(i).getR1()),
                        new Point2D(polygon2D.getPoint(next).getC1(),polygon2D.getPoint(next).getR1()));
                edge=edge.oriented();
                edge=edge.shortened();
                edges.add(edge);


            }
        }

        /**
         * Hledání Ymin a Ymax
         *
         * @MartinPortych
         */
        //find yMin,yMax
        int yMin=polygon2D.getPoint(0).getR1();
        int yMax=polygon2D.getPoint(0).getR1();
        for (int i=0;i<=listSize-1;i++){
            if (yMin>polygon2D.getPoint(i).getR1()){
                yMin=polygon2D.getPoint(i).getR1();
            }
            if (yMax<polygon2D.getPoint(i).getR1()){
                yMax=polygon2D.getPoint(i).getR1();
            }

        }
        //for y in range(yMin,yMax)
        //initialize list of intersections (x coordinates)
        /**
         * Vykreslování a vyplňování polygonu
         *
         * @MartinPortych
         */
        List<Integer> intersections=new ArrayList<Integer>();
        for (int y=yMin;y<=yMax;y++){
            intersections.clear();
            for (Edge edge:edges) {
                if(edge.hasInterection(y)){
                    intersections.add(edge.intersection(y));
                }
            }
            System.out.println(intersections.size());
            Collections.sort(intersections);
            if(intersections.size() % 2 != 0) System.err.println("Nalezen sudy pocer bodu! Pocet: " + intersections.size() + ", v y=" + y);
            if (intersections.size() % 2 != 0){}
            else {
            for (int x=0;x<intersections.size();x+=2){
                System.out.println(y);
                    liner.drawLine(img,intersections.get(x),y,intersections.get(x+1),y,areaPixel);
                //  polygon2D.addPoint(new Point2D(intersections.get(x),y));
                //  polygon2D.addPoint(new Point2D(intersections.get(x+1),y));
            }}

        }
        polygoner2D.drawPolygon(img,polygonPixel,liner,polygon2D);


        //for edge in edges
        //if intesections exists
        //add to our list of intersections
        //sort intersections
    }

}